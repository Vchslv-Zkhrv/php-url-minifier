# URL-minify application build on pure PHP (no frameworks, no javascript)



## Goals:

- [x] No-auth URL minify app
- [x] Rest API
    - For User (anonymous):
        - [x] Generate short link
        - [x] Decode short link
    - For admin:
        - [x] CRUD for links
        - [x] Aggregate redirects



## Scripts:

| Command                | Decription                                                |
| ---------------------- | --------------------------------------------------------- |
| `make add-admin`       | Register new admin and get `Authorization` token contents |
| `make drop-admin`      | Register admin from config                                |
| `remove-expired-links` | Delete expired links from the database                    |



## Public API:


### GET /api/minify.php

Query params:
- `full`: full link you need to enshort.

Will respond with text response with minified link on success.


### GET /api/expand.php

Query params:
- `short`: short link you need to expand.

Will respond with text response with full link on success.



## Admin API:

You must have an `Authorization` token to use that api.


### GET /api/links.php

Query params:
- `criteria`: list
- `sort`: list
- `limit`: number
- `offset`: number

All parameters are optional. Example:

```php
$query = [
    'criteria' => [ "created>'2024-07-03 13:00:00'" ], // UTC timezone
    'sort' => [ 'created', 'user DESC' ],
    'limit' => 1000
];

$url = "/api/links.php?" . http_build_query($query);
```

Response example:

```json
[
    {
        "full": "",
        "short": "",
        "user": "",
        "created": ""
    },
    {
        "full": "",
        "short": "",
        "user": "",
        "created": ""
    }
]
```


### PUT /api/links.php

Query params:
- `short`: short link (full url or only 10-character string). Link you want to modify

Body fields:
- `short`: new short link
- `full`: new full link

You must provide at least one field to change.


Request expample:

```HTTP
PUT /api/links.php?short=<linkToChange> localhost:8200/l/1 HTTP/1.1
Host: <somehost>
Authorization: Basic <TOKEN>

{
    "short": "",
    "full": ""
}
```

Response example:

```json
{
    "full": "",
    "short": "",
    "user": "",
    "created": ""
}
```


### DELETE /api/links.php

Query params:
- `short`: short link (full url or only 10-character string). Link you want to delete

Will respond with HTTP status 200 on Success


### GET /api/redirects.php

Query params:
- `criteria`: list
- `sort`: list
- `limit`: number
- `offset`: number

All parameters are optional. Example:

```php
$query = [
    'criteria' => [ "created>'2024-07-03 13:00:00'" ], // UTC timezone
    'sort' => [ 'created', 'user DESC' ],
    'limit' => 1000
];

$url = "/api/reddirects.php?" . http_build_query($query);
```

Response example:

```json
[
    {
        "short": "",
        "user": "",
        "created": ""
    },
    {
        "short": "",
        "user": "",
        "created": ""
    }
]
```

