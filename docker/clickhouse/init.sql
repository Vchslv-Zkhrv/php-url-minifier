CREATE TABLE IF NOT EXISTS link
(
    `created` DateTime,
    `user` FixedString(55),
    `short` FixedString(10),
    `full` String
)
ENGINE = MergeTree
ORDER BY short;


CREATE TABLE IF NOT EXISTS redirect
(
    `created` DateTime,
    `user` FixedString(55),
    `short` FixedString(10)
)
ENGINE = MergeTree
ORDER BY short;
