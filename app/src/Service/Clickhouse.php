<?php


class Clickhouse
{


    public string $response = '';
    public int    $status = 0;
    public bool   $success = false;


    public function __construct(

        private string $password,
        private string $user,
        private string $host,
        private string $port,

    ) { }


    private function sendRequest(string $query, string $data): bool
    {
        $session = curl_init();

        $params = [
            'query' => $query,
            'database' => 'default'
        ];

        curl_setopt_array($session, [
            CURLOPT_URL => "$this->host:$this->port/?" . http_build_query($params),
            CURLOPT_PORT => $this->port,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data),
                'Accept: *',
                'User-Agent: ',
                "X-ClickHouse-User: $this->user",
                "X-ClickHouse-Key: $this->password"
            ],
        ]);

        $this->response = curl_exec($session);
        $this->status = curl_getinfo($session, CURLINFO_HTTP_CODE);
        $this->success = $this->status == 200;

        curl_close($session);

        return $this->success;
    }


    public function insert(BaseEntity $entity): bool
    {
        $query = "INSERT INTO {$entity::getTablename()} FORMAT JSONEachRow";
        $data  = $entity->toJson();

        return $this->sendRequest($query, $data);
    }


    public function select(string $query)
    {
        $this->sendRequest($query, '');
        return $this->response;
    }


    /**
     * @param class-string<BaseEntity> $entity
     * @param array<int, string> $where
     * @param array<int, string> $orderBy
     * @return string
     */
    public function find(

        string $entity,
        array $where = [ ],
        array $orderBy = [ ],
        ?int $limit = null,
        ?int $offset = null

    ): string
    {
        $table = $entity::getTablename();
        $query = "SELECT * FROM $table ";

        if ($where) { $query .= 'WHERE ' . implode(', ', $where); }
        if ($orderBy) { $query .= 'ORDER BY ' . implode(', ', $orderBy); }

        if ($limit)  { $query .= " LIMIT $limit ";   }
        if ($offset) { $query .= " OFFSET $offset "; }

        $query .= " FORMAT JSONEachRow";

        return "[\n" . implode(",\n", array_filter(explode("\n", $this->select($query)))) . "\n]";
    }


    public function delete(BaseEntity $entity): bool
    {
        $table = $entity::getTablename();

        $where = [];
        foreach ($entity->toArray() as $key => $val) {
            array_push($where, "$key='$val'");
        }
        $where = implode(' AND ', $where);

        $query = "DELETE FROM $table WHERE $where";
        $this->sendRequest($query, '');

        return $this->success;
    }


}
