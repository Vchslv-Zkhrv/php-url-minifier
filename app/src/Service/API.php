<?php


require __DIR__ . '/Clickhouse.php';
require __DIR__ . '/../Entity/Link.php';
require __DIR__ . '/../Entity/Redirect.php';


class API
{


    public readonly array $config;
    public readonly string $userId;
    public readonly bool $isAdmin;

    public readonly Clickhouse $clickhouse;


    public function __construct()
    {
        $this->config = require(__DIR__ . '/../../config.php');
        $this->userId = $this->getUserId();

        $this->clickhouse = new Clickhouse(
            user: $this->config['clickhouse']['user'],
            host: $this->config['clickhouse']['host'],
            port: $this->config['clickhouse']['port'],
            password: $this->config['clickhouse']['password'],
        );

        if (array_key_exists('PHP_AUTH_USER', $_SERVER) && array_key_exists('PHP_AUTH_PW', $_SERVER)) {

            $user = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PW'];
            $this->isAdmin = array_key_exists($user, $this->config['admins']) && $this->config['admins'][$user] == $password;

        } else {

            $this->isAdmin = false;

        }
    }


    private function getUserId(): string
    {
        $id = null;
        $salt = $this->config['salt'];

        if      (array_key_exists('id', $_COOKIE)) { $id = $_COOKIE['id']; }
        else if (array_key_exists('id', $_SERVER)) { $id = $_SERVER['id'];  }

        if (is_string($id)) {

            $tail = substr($id, -32);
            $head = substr($id, 0, strlen($id)-32);

            if (md5($head . $salt) != $tail) {
                header('http/1.1 401 unauthorized', true, 401);
                setcookie('id', $id, time()-1);
                exit;
            }

        } else {

            $id = strval(time()) . uniqid();
            $id .= md5(($id . $salt));

        }

        setcookie('id', $id);
        header("X-AUTH: $id", true);

        return $id;
    }


    private function generateRandom(): string
    {
        $charset = '1234567890_abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $short = '';

        for ($i=0; $i<10; $i++) {
            $short .= $charset[random_int(0, 63)];
        }

        return $short;
    }


    public function minifyLink(string $full): string
    {
        $short = $this->generateRandom();

        $link = new Link(
            user: $this->userId,
            full: $full,
            short: $short,
            created: (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s'),
        );

        $success = $this->clickhouse->insert($link);

        try { $success = $this->clickhouse->insert($link); }
        catch (\Throwable $t) { $success = false; }

        return $success ? "{$this->config['host']}/l/$short" : 'Something went wrong';
    }


    public function expandLink(string $short): string
    {
        $created = (new \DateTime('now', new \DateTimeZone('UTC')))
            ->sub(new \DateInterval($this->config['ttl']))
            ->format('Y-m-d H:i:s');

        $query = "SELECT full FROM link WHERE short='$short' AND created>'$created' ORDER BY created LIMIT 1 FORMAT TabSeparated";
        return $this->clickhouse->select($query);
    }


    public function saveRedirect(string $short): void
    {
        $redirect = new Redirect(
            user: $this->userId,
            short: $short,
            created: (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s'),
        );

        $this->clickhouse->insert($redirect);
    }


    /**
     * @param array<int, string> $where
     * @param array<int, string> $orderBy
     * @return string
     */
    public function findLinks(

        array $where = [ ],
        array $orderBy = [ ],
        ?int $limit = null,
        ?int $offset = null

    ): string
    {
        return $this->clickhouse->find(Link::class, $where, $orderBy, $limit, $offset);
    }


    public function changeLink(

        string $oldShort,
        ?string $newShort = null,
        ?string $newFull = null

    ): string
    {
        if ($newShort && strlen($newShort)>10) { throw new \ErrorException("Short link too long"); }
        if ($this->findLinks(["short='$newShort'"])) { throw new \ErrorException("Short link already taken"); }

        $oldLinks = explode(
            "\n",
            $this->findLinks(
                where: ["short='$oldShort'"],
                orderBy: ["created ASC"]
            )
        );

        if (!$oldLinks) { throw new \ErrorException("No such link"); }

        $entity = Link::fromJson($oldLinks[0]);

        $success = $this->clickhouse->delete($entity);
        if (!$success) { throw new \ErrorException("Something went wrong"); }

        if ($newShort) { $entity->short = $newShort; }
        if ($newFull)  { $entity->full  = $newFull;  }

        $success = $this->clickhouse->insert($entity);
        if (!$success) { throw new \ErrorException("Something went wrong"); }

        $entity->short = "{$this->config['host']}/l/{$entity->short}";

        return $entity->toJson();
    }


    public function deleteLink(string $short): void
    {
        $links = explode(
            "\n",
            $this->findLinks(
                where: ["short='$short'"],
                orderBy: ["created ASC"]
            )
        );

        if (!$links) { throw new \ErrorException("No such link"); }

        $entity = Link::fromJson($links[0]);

        $success = $this->clickhouse->delete($entity);
        if (!$success) { throw new \ErrorException("Something went wrong"); }
    }


    /**
     * @param array<int, string> $where
     * @param array<int, string> $orderBy
     * @return string
     */
    public function findRedirects(

        array $where = [ ],
        array $orderBy = [ ],
        ?int $limit = null,
        ?int $offset = null

    ): string
    {
        return $this->clickhouse->find(Redirect::class, $where, $orderBy, $limit, $offset);
    }


}
