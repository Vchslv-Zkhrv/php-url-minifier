<?php


abstract class BaseEntity
{


    public abstract static function getTablename(): string;


    /**
     * @param array<string, string> $array
     */
    public static function fromArray(array $array): static
    {
        $data = [];

        foreach ((new \ReflectionMethod(static::class, '__construct'))->getParameters() as $param) {
            $name = $param->name;
            $data[$name] = $array[$name];
        }

        return new static(...$data);
    }


    public static function fromJson(string $json): static
    {
        return static::fromArray(json_decode($json, true));
    }


    public function toArray(): array
    {
        $fields = [];

        foreach ((new \ReflectionMethod(static::class, '__construct'))->getParameters() as $param) {
            $name = $param->name;
            $fields[$name] = $this->$name;
        }

        return $fields;
    }


    public function toJson(): string
    {
        return json_encode($this->toArray());
    }


}
