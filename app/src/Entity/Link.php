<?php


require __DIR__ . '/BaseEntity.php';


class Link extends BaseEntity
{


    public static function getTablename(): string
    {
        return 'link';
    }


    public function __construct(

        public string $user,
        public string $full,
        public string $short,
        public string $created

    ) { }


}
