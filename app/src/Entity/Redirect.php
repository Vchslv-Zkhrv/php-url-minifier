<?php


class Redirect extends BaseEntity
{


    public static function getTablename(): string
    {
        return 'redirect';
    }


    public function __construct(

        public string $user,
        public string $short,
        public string $created

    ) { }


}
