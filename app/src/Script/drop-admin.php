<?php


/** @var array */
$config = require(__DIR__ . "/../../config.php");
$admins = array_keys($config['admins']);


echo "\n";
echo "Existing admins:\n";
for ($i=1; $i<=count($admins); $i++) { echo "\t$i. {$admins[$i-1]}\n"; }
echo "\n";


$input = trim(readline("\e[0;33mEnter admin number to delete:\e[0m "));
if (!$input) { exit(); }
if (!is_numeric($input)) { echo "\e[0;31mNot a number\e[0m\n"; exit(); }


$number = intval($input);
if ($number > count($admins)) { echo "\e[0;31mNo such number\e[0m\n"; exit(); }


$login = $admins[$number-1];
unset($config['admins'][$login]);

$export = var_export($config,  true);

file_put_contents(
    __DIR__ . "/../../config.php",
    "<?php\n\nreturn $export;"
);


echo "\n";
echo "Admin deleted\n";
echo "\n";
