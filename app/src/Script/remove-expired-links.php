<?php


require __DIR__ . "/../Service/API.php";


$api = new API();


$created = (new \DateTime('now', new \DateTimeZone('UTC')))
    ->sub(new \DateInterval($api->config['ttl']))
    ->format('Y-m-d H:i:s');


echo "\n";
echo " - [ ] Looking for links created before $created\n";


$response = $api->findLinks(["created<'$created'"]);
$response = $response ? $response : '[ ]';


/** @var Link[] */
$links = array_map(
    function (array $row) { return Link::fromArray($row); },
    json_decode($response, true)
);

$count = count($links);


echo " - [ ] Deleting $count links \n";


foreach ($links as $link) {
    $api->clickhouse->delete($link);
}


echo " - [x] All links deleted \n";
echo "\n";
