<?php


/** @var array */
$config = require(__DIR__ . "/../../config.php");
$admins = array_keys($config['admins']);


echo "\n";
echo "Existing admins:\n";
foreach ($admins as $login) { echo "\t- $login\n"; }
echo "\n";


$login = trim(readline("\e[0;33mEnter new admin login:\e[0m "));
if (!$login) { exit(); }

if (in_array($login, $admins)) { echo "\e[0;31mLogin is taken already\e[0m\n"; exit(); }

$password = trim(readline("\e[0;33mEnter new admin password:\e[0m "));
if (!$password) { exit(); }

$token = "Basic " . base64_encode("$login:$password");

$config['admins'][$login] = $password;
$export = var_export($config,  true);

file_put_contents(
    __DIR__ . "/../../config.php",
    "<?php\n\nreturn $export;"
);


echo "\n";
echo "Admin added. Authorization token: \e[1;32m$token\e[1;32m\n";
echo "\n";
