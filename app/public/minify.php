<?php


require "../src/Service/API.php";
$api = new API();

$short = $api->minifyLink($_GET['full']);

echo <<<HTML
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>URL minifier</title>
    <link href="styles.css" rel="stylesheet" />
</head>
<body>
<main>
    <form action="index.php">
        <h1>Your short link:</h1>
        <pre id="code" onClick="navigator.clipboard.writeText(this.textContent)"> $short </pre>
        <input type="submit" value="Go back">
    </form>

</main>
</body>
</html>
HTML;
