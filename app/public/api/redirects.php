<?php


require "../../src/Service/API.php";
$api = new API();


$method = $_SERVER['REQUEST_METHOD'];


function close(string $message = '', int $status = 400)
{
    http_response_code($status);
    echo $message;
    exit();
}


if (!$api->isAdmin) {

    close('Bad credentials', 401);

} else if ($method == 'GET') {

    $where   = isset($_GET['criteria']) ? $_GET['criteria'] : [ ];
    $orderBy = isset($_GET['sort'])     ? $_GET['sort']     : [ ];
    $limit   = isset($_GET['limit'])    ? $_GET['limit']    : null;
    $offset  = isset($_GET['offset'])   ? $_GET['offset']   : null;

    echo $api->findRedirects($where, $orderBy, $limit, $offset);

} else {

    close('Method not allowed', 405);

}
