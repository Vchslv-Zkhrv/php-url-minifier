<?php


require "../../src/Service/API.php";
$api = new API();


if (array_key_exists('short', $_GET)) {

    $short = $_GET['short'];
    $short = pathinfo($short)['basename'];

    $full = $api->expandLink($short);

    if ($full) { echo $full;              }
    else       { http_response_code(404); }

} else {

    http_response_code(400);

}
