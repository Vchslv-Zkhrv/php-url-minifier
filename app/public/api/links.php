<?php


require "../../src/Service/API.php";
$api = new API();


$method = $_SERVER['REQUEST_METHOD'];


function close(string $message = '', int $status = 400)
{
    http_response_code($status);
    echo $message;
    exit();
}


if (!$api->isAdmin) {

    close('Bad credentials', 401);

} else if ($method == 'GET') {

    $where   = isset($_GET['criteria']) ? $_GET['criteria'] : [ ];
    $orderBy = isset($_GET['sort'])     ? $_GET['sort']     : [ ];
    $limit   = isset($_GET['limit'])    ? $_GET['limit']    : null;
    $offset  = isset($_GET['offset'])   ? $_GET['offset']   : null;

    echo $api->findLinks($where, $orderBy, $limit, $offset);

} else if ($method == 'PUT') {

    if (!array_key_exists('short', $_GET)) {  close("Set `short` query param to identify link you are going to change"); }
    $oldShort = pathinfo($_GET['short'])['basename'];

    $body = file_get_contents('php://input');
    if (!is_string($body)) { close("Empty request body"); }

    $body = json_decode($body, true);
    if (!is_array($body)) { close("Request body is not JSON"); }

    $newShort = array_key_exists('short', $body) ? $body['short'] : null;
    $newFull = array_key_exists('full', $body) ? $body['full'] : null;

    if (!$newFull && !$newShort) { close("No fields to change set"); }

    try { echo $api->changeLink($oldShort, $newShort, $newFull); }
    catch (\Throwable $e) { close($e->getMessage(), 409); }

} else if ($method == 'DELETE') {

    if (!array_key_exists('short', $_GET)) {  close("Set `short` query param to identify link you are going to change"); }
    $short = pathinfo($_GET['short'])['basename'];

    try { $api->deleteLink($short); }
    catch (\Throwable $e) { close($e->getMessage(), 409); }

} else {

    close('Method not allowed', 405);

}
