<?php


require "../../src/Service/API.php";
$api = new API();


if (array_key_exists('full', $_GET)) {

    echo $api->minifyLink($_GET['full']);

} else {

    http_response_code(400);

}
