<?php


require "../src/Service/API.php";
$api = new API();


$short = pathinfo($_SERVER['REQUEST_URI'])['basename'];
$full = $api->expandLink($short);


if ($full) {

    $api->saveRedirect($short);

    header("Location: $full", true, 301);
    exit();

} else {

    echo <<<HTML

    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Link not found</title>
    </head>
    <body style="height:100vh;padding:0;margin:0;display:flex;">
        <main style="margin:auto;">
            <h1 style="font-size:66px;margin:0;">404</h1>
            <h2 style="color:red;margin:0;">Link not found or expired</h2>
        </main>
    </body>
    </html>

    HTML;

}
