<?php


return [
    'clickhouse' => [
        'host' => 'http://clickhouse',
        'port' => '8123',
        'user' => 'php',
        'password' => 'password',
        'database' => 'minify'
    ],
    'host' => 'localhost:8200',
    'ttl' => 'P1Y', // links time-to-live (php dateinterval format)
    'salt' => 'fldesksdq34313kj',
    'admins' => [
        'admin1' => 'password1',
        'admin2' => 'password2',
    ]
];
