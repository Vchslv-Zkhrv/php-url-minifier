setup:
	touch .env
	docker compose run --rm composer install --ignore-platform-reqs
	cp app/example-config.php config.php
	@echo "\e[44m                                                \e[0m"
	@echo "\e[44m  Edit .env and app/config.php to finish setup  \e[0m"
	@echo "\e[44m                                                \e[0m"
run:
	docker compose up -d php-fpm www clickhouse
down:
	docker compose down --remove-orphans
logs:
	docker compose logs
remove-expired-links:
	docker compose run --rm script /usr/local/bin/php /var/www/app/src/Script/remove-expired-links.php
add-admin:
	docker compose run --rm script /usr/local/bin/php /var/www/app/src/Script/add-admin.php
drop-admin:
	docker compose run --rm script /usr/local/bin/php /var/www/app/src/Script/drop-admin.php
